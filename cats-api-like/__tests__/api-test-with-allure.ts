import type { Reporter } from 'jest-allure/dist/Reporter';
import { Severity } from 'jest-allure/dist/Reporter';
import Client from '../../dev/http-client';

declare const reporter: Reporter;

const HttpClient = Client.getInstance();

describe('API лайков - демо с Allure', () => {
  it('Метод получения рейтинга котов - демо с Allure', async () => {
    reporter.severity(Severity.Blocker);
    reporter.description('Это тест на проверку рейтинга котиков');

    reporter.startStep('Делаю запрос на получение рейтинга котиков');
    const response = await HttpClient.get('likes/cats/rating', {
      responseType: 'json',
    });
    reporter.addAttachment(
      'testAttachment',
      JSON.stringify(response.body, null, 2),
      'application/json'
    );
    reporter.endStep();

    reporter.startStep('Делаю проверку статуса и структуры ответа');
    expect(response.statusCode).toEqual(200);

    expect(response.body).toEqual({
      likes: expect.arrayContaining([
        expect.objectContaining({
          id: expect.any(Number),
          name: expect.any(String),
          likes: expect.any(Number),
        }),
      ]),
      dislikes: expect.arrayContaining([
        expect.objectContaining({
          id: expect.any(Number),
          name: expect.any(Number),
          dislikes: expect.any(Number),
        }),
      ]),
    });
    reporter.endStep();
  });
});
