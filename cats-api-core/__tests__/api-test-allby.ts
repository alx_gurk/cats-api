// Импорт класса Client из файла по указанному пути для работы с HTTP-клиентом
import Client from '../../dev/http-client';

// Получение экземпляра клиента HttpClient с помощью метода getInstance() из класса Client
const HttpClient = Client.getInstance();

// Определение параметров запроса как объекта
const queryParams = {
    limit: 1,
    order: 'desc',
    gender: 'male'
};

// Преобразует объект queryParams в массив, содержащий массивы с ключами и значениями пар ключ-значение
const queryString = Object.entries(queryParams)
    .map(([key, value]) => `${key}=${value}`) // Проходит по каждой паре ключ-значение в массиве и создает новый массив, содержащий строки вида "ключ=значение" для каждой пары.
    .join('&'); // Соединяет все ключи и значения в строку запроса, разделяя их символом &

// Построение строки запроса с использованием динамических параметров
const endpoint = `core/cats/allByLetter?${queryString}`;

it.skip('Получение списка котов сгруппированных по группам', async () => {
    const response = await HttpClient.get(endpoint, {
        responseType: 'json'
    });
    
    expect(response.statusCode).toEqual(200);

    expect(response.body).toEqual({
        count_all: expect.any(Number),
        count_output: expect.any(Number),
        groups: expect.arrayContaining([
            expect.objectContaining({
                cats: expect.arrayContaining([
                    expect.objectContaining({
                        count_by_letter: expect.any(String),
                        description: expect.any(String),
                        dislikes: expect.any(Number),
                        gender: expect.any(String),
                        id: expect.any(Number),
                        likes: expect.any(Number),
                        name: expect.any(String),
                        tags: null
                    })
                ]),
                count_by_letter: expect.any(Number),
                count_in_group: expect.any(Number),
                title: expect.any(String)
            })
        ])
    });
});

