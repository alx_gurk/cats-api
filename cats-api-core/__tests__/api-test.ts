// Импорт класса Client из файла по указанному пути для работы с HTTP-клиентом
import Client from '../../dev/http-client';

// Импорт типов CatMinInfo и CatsList из файла по указанному пути для использования в коде
import type { CatMinInfo, CatsList } from '../../dev/types';

// Инициализация массива cats с информацией о котах, содержащихся в виде объектов CatMinInfo
const cats: CatMinInfo[] = [{ name: 'Чучкаавто', description: '', gender: 'female' }];

// Объявление переменной fakeId со значением fakeId (фиктивный идентификатору)
const fakeId = 'fakeId';

// Объявление переменной catId без присвоения значения
let catId;

// Получение экземпляра клиента HttpClient с помощью метода getInstance() из класса Client
const HttpClient = Client.getInstance();


describe('API поиска котика Чучкиавто по существующему и некорректному id', () => {
    // Хук выполнения ассинхронных действий перед выполнением всех тестов
    beforeAll(async () => {
    // Отправка POST-запроса на добавление котика на эндпоинт 'core/cats/add' с данными cats (из переменной CatMinInfo)    
    try {
      const add_cat_response = await HttpClient.post('core/cats/add', {
        responseType: 'json',
        json: { cats },
      });

      // Проверка наличия id в ответе и присвоение id переменной catId
      if ((add_cat_response.body as CatsList).cats[0].id) {
        catId = (add_cat_response.body as CatsList).cats[0].id;
      } else throw new Error('Не удалось получить id тестового котика Чучкаавто!');
    } catch (error) {
      // Обработка ошибки в случае неудачного создания котика для автотестов
      throw new Error('Не удалось создать котика Чучкаавто для автотестов!');
    }
  });


  // Хук выполнения асинхронных действий (удаление котика Чучкаавто) после завершения всех тестов
  afterAll(async () => {
    await HttpClient.delete(`core/cats/${catId}/remove`, {
      responseType: 'json',
    });
  });


  it.skip('Найти существующего котика Чучкиавто по id', async () => {
    // Отправка GET-запроса для получения информации о котике по его идентификатору catId
    const response = await HttpClient.get(`core/cats/get-by-id?id=${catId}`, {
        responseType: 'json',
    });
    
    expect(response.body as any).toEqual
    // Ожидаемые характеристики котика
    const expectedCat = {
        id: catId,
        ...cats[0], // Деструктуризация
        likes: 0,
        dislikes: 0,
        tags: null
    };


    // Проверка соответствия полученной информации о котике ожидаемым характеристикам
    expect(response.body as any).toEqual(expect.objectContaining({ cat: expectedCat }));
});

it.only('Найти котика по некорректному id', async () => {
    // Ожидаем, что запрос HttpClient.get вернет ошибку
    await expect(HttpClient.get(`core/cats/get-by-id?id=${fakeId}`, {
        responseType: 'json',
    })).rejects.toThrowError('Response code 400 (Bad Request)');
    // Проверяем, что ошибка содержит сообщение 'Response code 400 (Bad Request)'
});


});


