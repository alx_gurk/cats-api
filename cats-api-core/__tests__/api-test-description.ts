// Импорт класса Client из файла по указанному пути для работы с HTTP-клиентом
import Client from '../../dev/http-client';

// Импорт типов CatMinInfo и CatsList из файла по указанному пути для использования в коде
import type { CatMinInfo, CatsList } from '../../dev/types';

// Инициализация массива cats с информацией о котах, содержащихся в виде объектов CatMinInfo
const cats: CatMinInfo[] = [{ name: 'Чучкаавто', description: '', gender: 'female' }];

// Объявление переменной catId без присвоения значения
let catId;

// Получение экземпляра клиента HttpClient с помощью метода getInstance() из класса Client
const HttpClient = Client.getInstance();


describe('API поиска котика Чучкиавто по id', () => {
    // Хук выполнения ассинхронных действий перед выполнением всех тестов
    beforeAll(async () => {
    // Отправка POST-запроса на добавление котика на указанный эндпоинт 'core/cats/add' с данными cats (из переменной CatMinInfo)  
    try {
      const add_cat_response = await HttpClient.post('core/cats/add', {
        responseType: 'json',
        json: { cats },
      });

      // Проверка наличия id в ответе и присвоение id переменной catId
      if ((add_cat_response.body as CatsList).cats[0].id) {
        catId = (add_cat_response.body as CatsList).cats[0].id;
      } else throw new Error('Не удалось получить id тестового котика Чучкаавто!');
    } catch (error) {
      // Обработка ошибки в случае неудачного создания котика для автотестов
      throw new Error('Не удалось создать котика Чучкаавто для автотестов!');
    }
  });


  // Хук выполнения ассинхронных действий после завершения всех тестов
  afterAll(async () => {
    await HttpClient.delete(`core/cats/${catId}/remove`, {
      responseType: 'json',
    });
  });


it.skip('Добавить описание для котика Чучкаавто', async () => {
    // Описание котика
    const catDescription = "Чучкаавто - лучшая кошка на свете!";
    // Отправка POST запроса для сохранения описания котика
    const response = await HttpClient.post('core/cats/save-description', {
        responseType: 'json',
        json: {
            catId: catId,
            catDescription: catDescription
        }
    });

    // Проверка статус кода ответа
    expect(response.statusCode).toEqual(200);

    // Проверка тела ответа
    expect(response.body).toEqual({
        id: catId,
        name: 'Чучкаавто',
        description: catDescription,
        gender: 'female',
        likes: 0,
        dislikes: 0,
        tags: null
    });
});
});
