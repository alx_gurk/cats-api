// Импорт библиотеки got для работы с HTTP-запросами
import got from 'got';

// Экспорт класса Client для использования в других файлах
export default class Client {
  /*
   * Метод получения экземпляра Got с настройками prefixUrl, заданными из переменной окружения STAND
   */
  static getInstance() {
    
    // Создание экземпляра Got с расширенными настройками prefixUrl
    
    const customGot = got.extend({
      prefixUrl: process.env.STAND,
    });

    // Возвращение настроенного экземпляра Got
    return customGot;
  }
}
