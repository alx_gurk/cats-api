/**
 * Набор минимальной информации для добавления кота
 * @property name - имя
 * @property description - описание
 * @property gender - пол
 */
export type CatMinInfo = {
  name: string;
  description: string;
  gender: 'male' | 'female' | 'unisex';
};

/**
 * Полная информация о коте
 * @property name - имя
 * @property description - описание
 * @property gender - пол
 * @property id - id
 * @property tags - теги
 * @property likes - количество лайков
 * @property dislikes - количество дизлайков
 * @property [message=] - сообщение
 */
export type Cat = CatMinInfo & {
  id: number;
  tags: string;
  likes: number;
  dislikes: number;
  message?: string;
};

/**
 * Ответ при успешном добавлении кота
 */
export type CatsList = { cats: Cat[] };